package com.example.activity1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class hasilAct extends AppCompatActivity {
    TextView hasil, operasi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        hasil = findViewById(R.id.Hasil);
        operasi = findViewById(R.id.Operasi);

        Intent in=getIntent();
        String hsl =in.getStringExtra("result");
        String opr =in.getStringExtra("operasi");

        hasil.setText(hsl);
        operasi.setText(opr);

    }
}
